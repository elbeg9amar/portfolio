import styled from "styled-components"

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
`

export const ProContainer = styled.div`
  text-align: center;
  margin: 2%;
  width: 400px;
  img {
    max-width: 100%;
  }
  a {
    color: var(--clr-primary-5);
    font-size: 1.25rem;
    margin-right: 0.5rem;
    transition: var(--transition);
  }
`

export const Title = styled.h1`
  text-align: center;
`