import React from 'react';
import { Link } from "gatsby"
import {FaAlignRight} from 'react-icons/fa';

import logo from '../../images/logo.jpg'

import PageLinks from '../../localData/links';

import {
    NavBar,
    NavCenter,
    NavHeader,
    NavButton
} from './Header.styled';

function Header() {
    return (
        <NavBar>
            <NavCenter>
                <NavHeader>
                    <Link to='/'><img src={logo} alt="logo" /></Link>
                    <NavButton type="button" className="toggle-btn">
                        <FaAlignRight></FaAlignRight>
                    </NavButton>
                </NavHeader>
                <PageLinks styleClass="nav-links"/>
                
            </NavCenter>
        </NavBar>
    )
};


export default Header;
