
import React from "react";

//importing components
import Header from '../Header/Header';
import Footer from '../Footer/Footer';


import "./layout.css";

const Layout = ({ children }) => {
  // const {user,firebase,loading} = useAuth();

  return (
      <>
      <Header/>
        <main>{children}</main>
      <Footer/>
      </>
  )
}



export default Layout
