import styled from "styled-components"

export const Container = styled.div`
  display: flex;
  flex-wrap: wrap;
  flex-direction:row;
  justify-content: space-evenly;
`

export const ProContainer = styled.div`
  text-align: center;
  margin: 2%;
  border: 5px solid #ddd;
  width: 400px;
  font-family:sans-serif;
  h3, p  {
    font-size:1.2rem;
    text-transform:none;
  }
  
`

