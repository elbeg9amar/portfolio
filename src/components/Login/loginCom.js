import React from 'react';

import { Container, ProContainer} from './logenCom.styled';


function LoginCom({data}) {
    
    const {email,message, firstName} = data 
    return (
        <>
            <Container>
                <ProContainer>
                    <h3>Name:{firstName}</h3>
                    <h3>Email:{email}</h3>
                    <p>Message: {message}</p>
                </ProContainer>
            </Container>
        </>
    )
}



export default LoginCom;
