import React from 'react';
import SocialLinks from '../../localData/socialLinks';

import {
    
    FooterStyled,
    FooterContainer
} from './Footer.styled'

function Footer() {
    return (
        <FooterStyled>
            <FooterContainer>
                <SocialLinks/>
                <h4>
                    copyright&copy;{new Date().getFullYear()}
                    <span> Elbeg </span> all rights reserved
                </h4>
            </FooterContainer>
        </FooterStyled>
        
    )
}

export default Footer
