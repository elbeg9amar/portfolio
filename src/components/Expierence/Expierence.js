import React from "react"

import { ExpContainer, ExpBox} from "./Expierence.styled"

const Expierence = ({ expData }) => {
    return (
        <>
        <ExpContainer>
            <h1 className="title">Exprience</h1>
            <ExpBox>
            {expData.map(exp => {
                return (
                <div className="exp" key={exp.id}>
                    <div className="info">
                    <div>
                        <h2 className="company">{exp.company}</h2>
                    </div>
                    <div className="date-and-position">
                        <p className="date">{exp.date}</p>
                        <p className="position">{exp.position}</p>
                    </div>
                    </div>

                    <div className="details">
                    <p className="about">{exp.text}</p>
                    </div>
                </div>
                )
            })}
            </ExpBox>
        </ExpContainer>
        </>
    )
}

export default Expierence
