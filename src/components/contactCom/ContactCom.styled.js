import styled from 'styled-components';


export const ContactSection = styled.section`
    background: linear-gradient(rgba(0,0,0,0.6),rgba(0,0,0,0.9)),url('https://images.unsplash.com/photo-1471107340929-a87cd0f5b5f3?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1266&q=80');
    background-size: cover;
    background-position: center;
    height: 100%;
    width: 100% ;
    padding-bottom: 2%;
    @media (max-width: 768px){
        display: block;
        width: 100%;
        text-align: center;
    }
`;

export const ContactContainer = styled.div`
    h2 {
        padding-top:5%;
        text-align: center;
        text-decoration: underline;
        /* text-decoration-color:red; */
        text-underline-position: under;
        color: rgb(238, 235, 235);
        letter-spacing: 2px;
    }
    p{
        text-align: center; 
        width: 70%; 
        margin-left: auto;
        margin-right: auto; 
        padding-bottom: 3%;
        color: #fff;
        letter-spacing:3px;
    }
`;
export const FormInfo = styled.span`
        font-size: 16px;
        font-style: italic;
        color: white;
        letter-spacing: 2px;
`;
export const ContactInfo = styled.div`
    display:flex;
    flex-direction:column;
    align-items:center;
    width:35rem;
`;
export const ContactForm = styled.div`
    display: grid;
    grid-template-columns: auto auto;
    @media (max-width: 768px){
        display: block;
        width: 100%;
        text-align: center;
    }
    .fa{
        /* color: red; 
        color: #fff;  */
        font-size: 22px; 
        padding: 3%;
        background-color: none;
        border-radius: 80%;
        margin: 2%;
        /* border: 2px solid #fff;  */
        cursor: pointer;
        border:2px solid rgb(190, 190, 190);
        color: rgb(190, 190, 190);
        &:hover {
            cursor: pointer;
            border:2px solid white;
            color: white;
        }
    }
    input{
        padding: 10px;
        margin:10px;
        width: 70%;
        background-color:rgba(136, 133, 133, 0.5);
        color: white;
        border: none;
        outline:none;
    }

    input::placeholder{
        color: white;
    }
    
    textarea{
        padding: 10px;
        margin: 10px;     
        width: 70%;
        background-color:rgba(136, 133, 133, 0.5);
        color: white;
        border: none;
        outline:none;
    }
    textarea::placeholder{
        color: white;
    }

    .submit{
        width: 40%;
        background: none;
        padding: 4px;
        outline: none;
        /* border: 1px solid #fff;
      color: #fff; */
        font-size: 13px;
        font-weight: bold;
        letter-spacing: 2px;
        height: 33px;
        text-align: center;
        cursor: pointer;
        letter-spacing: 2px;
        margin-left: 3%;
        border:2px solid rgb(190, 190, 190);
        color: rgb(190, 190, 190);
        @media (max-width: 768px){
        width: 60%;
    }
    }

    .submit:hover{
        border: 1px solid #fff;
        color: #fff;
        cursor: pointer;
    }
`;

