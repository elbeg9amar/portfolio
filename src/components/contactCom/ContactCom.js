import React, {useState} from 'react';
import {addToFirestore} from '../../firebase.utils/firebase';
import {FaMapMarker, FaPhoneAlt, FaEnvelope} from 'react-icons/fa';
import { useNavigate } from "@reach/router";
import { 
    ContactSection,
    ContactContainer,
    ContactForm,
    FormInfo,
    ContactInfo
} from './ContactCom.styled';
const initialValue = {
    firstName:'',
    lastName:'',
    email:'',
    subject:'',
    message:''
}
function ContactCom() {
    const [formValues,setFormValues] = useState(initialValue)
    const navigater = useNavigate();
    function handleChanges (e) {
        e.persist();
        setFormValues(currentValues => ({
            ...currentValues,
            [e.target.name]: e.target.value
        }))
    };

    const onSubmit = async(e) => {
        e.preventDefault();
        const obj = {
            firstName:formValues.firstName,
            lastName:formValues.lastName,
            email:formValues.email,
            subject:formValues.subject,
            message:formValues.message,
        };

        await addToFirestore(obj)

        navigater("/login");
        
        
    };
    return (
        <>
        <ContactSection>
            <ContactContainer>
                <h2>Contact</h2>
                <p>Keep in Touch</p>
                <ContactForm>

                    <ContactInfo>
                        <FaMapMarker className="fa"></FaMapMarker>
                        <FormInfo>  192 City Boston Amercia #345</FormInfo>
                        <FaPhoneAlt className="fa"></FaPhoneAlt>
                        <FormInfo> +92 00034567890</FormInfo>
                        <FaEnvelope className="fa"></FaEnvelope>
                        <FormInfo>  JhonDoe12@Gmail.com</FormInfo>
                    </ContactInfo>

                    <div>
                        <form onSubmit={onSubmit}>
                            <input 
                                name="firstName" 
                                placeholder="Your Name" 
                                required 
                                value={formValues.firstName}
                                onChange={handleChanges}
                            />
                            <input 
                                name="lastName"  
                                placeholder="Last Name" 
                                value={formValues.lastName}
                                onChange={handleChanges}
                            />
                            <input 
                                name="email" 
                                placeholder="Email" 
                                required 
                                value={formValues.email}
                                onChange={handleChanges}
                            />
                            <input 
                                name="subject"  
                                placeholder="Subject of this message" 
                                value={formValues.subject}
                                onChange={handleChanges}
                            />
                            <textarea 
                                name="message" 
                                placeholder="Message" 
                                rows="5" 
                                required 
                                value={formValues.message}
                                onChange={handleChanges}
                            ></textarea>
                            <button className="submit">Send Message</button>
                        </form>
                    </div>
                </ContactForm>
            </ContactContainer>
        </ContactSection>
        </>
    );
};

export default ContactCom;
