import React from 'react';
import {Link} from "gatsby";

import {
    IntroBox,
    IntroContainer,
} from './Intro.styled';


const Intro = ({about,aboutData,skills}) => {
    const {name, position, aboutTitle, aboutText ,localImage} = aboutData;
    if (!about) {
        return (
            <IntroBox>
                <IntroContainer>
                    <img src={localImage.publicURL} alt="pic" className="img"/>
                    <h1>{name}</h1>
                    <h5>{position}</h5>
                    <Link to="contact" className="btn">
                        Contact me 
                    </Link>
                </IntroContainer>
            </IntroBox>
        );
    } else {
        return (
            <IntroBox>
                <IntroContainer>
                    <h1>{aboutTitle}</h1>
                    <img src={localImage.publicURL} alt="pic" className="img"/>
                    <h1>{name}</h1>
                    <h5>{position}</h5>
                    <p>{aboutText}</p>
                    {skills.map(skill => {
                        return  <div key={skill.id}>
                                <h3>{skill.name}</h3>
                                <h4>{skill.value}</h4>
                                </div>
                    })}
                </IntroContainer>
            </IntroBox>
        );
    };
};

export default Intro;
