import styled from 'styled-components';

export const IntroBox = styled.div`
    margin: 0;
    padding: 0;
    background: url('https://images.unsplash.com/photo-1498050108023-c5249f4df085?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1352&q=80') no-repeat;
    background-size: cover;
`;

export const IntroContainer = styled.div`
    width: 450px;
    background: rgba(0, 0, 0, 0.4);
    padding: 40px;
    text-align: center;
    margin: auto;
    margin-top: 5%;
    color: white;
    font-family: 'Century Gothic',sans-serif;
    .img {
        border-radius: 50%;
        width: 200px;
        height: 200px;
        display: block;
        margin-left: auto;
        margin-right: auto;
    }

    h1{
        font-size: 40px;
        letter-spacing: 4px;
        font-weight: 100;
    }
    h5{
        font-size: 20px;
        letter-spacing: 3px;
        font-weight: 100;
    }
    p{
        text-align: justify; 
    }
    .btn {
        text-transform: uppercase;
        background: var(--clr-primary-5);
        color: var(--clr-primary-9);
        padding: 0.375rem 0.75rem;
        letter-spacing: var(--spacing);
        display: inline-block;
        font-weight: 700;
        -webkit-transition: var(--transition);
        transition: var(--transition);
        font-size: 0.875rem;
        border: 2px solid transparent;
        cursor: pointer;
        box-shadow: 0 1px 3px rgba(0, 0, 0, 0.2);
        border-radius: var(--radius);
        &:hover{
            color: var(--clr-primary-1);
            background: black;
        }
    }
    @media screen and (min-width: 500px) {
        margin: auto;
        p{
            text-align: center; 
        }
    }
`;
