import React from 'react';
import {graphql} from "gatsby";

//importing components
import Layout from '../components/Layout/layout';
import Intro from '../components/Intro/Intro';
import Expierence from '../components/Expierence/Expierence'

const  AboutPage = (props) => {
    const aboutData = props.data.allAbout.edges[0].node;
    const skills = props.data.allSkill.edges.map(ed => (ed.node));
    const expData = props.data.allExprience.edges.map(exp => (exp.node))
    
    
    
    return (
        <Layout>
            <Intro about="about" aboutData={aboutData} skills={skills}/>
            <Expierence expData={expData}/>
        </Layout>
    );
};

export const query = graphql`
    {
    allAbout {
        edges {
        node {
            position
            localImage {
                publicURL
            }
            name
            aboutTitle
            aboutText
        }
        }
    }
    allSkill {
        edges {
        node {
            value
            name
            id
        }
        }
    }
    allExprience {
        edges {
        node {
            company
            date
            position
            id
            text
        }
        }
    }
}
`

export default AboutPage;
