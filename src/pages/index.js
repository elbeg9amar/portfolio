import React from "react";

//importing components
import Layout from "../components/Layout/layout";
import Intro from "../components/Intro/Intro";
import ProjectsCom from '../components/Project/ProjectCom'



const IndexPage = (props) => {
  const aboutData = props.data.allAbout.edges[0].node;
  const projectData = props.data.allProjects.edges.map(pro => (pro.node))

  return (
    <>
    <Layout>
      <Intro aboutData={aboutData}/>
      <ProjectsCom  projectData={projectData}/>
    </Layout>
    </>
  )
}
export const query = graphql`
{
  allAbout {
    edges { 
      node {
        position
        localImage {
          publicURL
        }
        name
      }
    }
  }
  allProjects(limit: 3) {
    edges {
      node {
        title
        picUrl
        github
        description
      }
    }
  }
}
`
export default IndexPage
