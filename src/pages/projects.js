import React from 'react';

import Layout from '../components/Layout/layout';
import ProjectsCom from '../components/Project/ProjectCom'
function Projects(props) {
    const projectData = props.data.allProjects.edges.map(pro => (pro.node))
    return (
        <Layout>
        <ProjectsCom  projectData={projectData}/>
            
        </Layout>
    );
};

export const query = graphql`
    {
        allProjects {
        edges {
            node {
            description
            github
            picUrl
            title
            }
        }
        }
    }
`

export default Projects
