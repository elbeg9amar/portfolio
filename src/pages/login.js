import React, { useEffect, useState } from 'react';
import styled from "styled-components"
import {firestore} from '../firebase.utils/firebase'
import Layout from '../components/Layout/layout';
import LoginCom from '../components/Login/loginCom';
const CommentDiv = styled.div`
    display:flex;
    flex-direction:row;
    justify-content:space-evenly;
    flex-wrap:wrap;
`;
const Login = () => {
    const [data, setData] = useState(null)

    const getData = async() => {
        const collectionRef = firestore.collection('messages')
        const snapshot = await collectionRef.get()
        const messagesData = snapshot.docs.map(doc => doc.data())
        return setData(messagesData)
    };
    
    useEffect(() => {
        getData()
    },[])

    if(!data){
        return (
            <>
                <Layout>
                    <h1 style={{
                        textAlign:"center"
                    }}>Loading ...</h1>
                </Layout>
            </>
        )  
    } else {
        return (
            <>
                <Layout>
                    <h1 style={{
                        textAlign:"center"
                    }}>Messages</h1>
                    <CommentDiv>
                        {
                            data.map(d => (
                                <LoginCom key={d.id} data={d}/>
                            ))
                        }
                    </CommentDiv>
                </Layout>
            </>
        )  
    }
};


export default Login;