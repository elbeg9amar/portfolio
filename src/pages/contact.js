import React from 'react';

import Layout from '../components/Layout/layout';
import ContactCom from '../components/contactCom/ContactCom'

function Contact() {
    return (
        <Layout>
            <ContactCom />
        </Layout>
    )
}

export default Contact;
