import firebase from 'firebase'
import 'firebase/firebase-app';
import 'firebase/firebase-storage';

const config = {
    apiKey: "AIzaSyDzcRx2VHS4V561mXK78dpihQibHX_0N8g",
    authDomain: "portfolio-25c63.firebaseapp.com",
    projectId: "portfolio-25c63",
    storageBucket: "portfolio-25c63.appspot.com",
    messagingSenderId: "732653245462",
    appId: "1:732653245462:web:ac62b30d67b44abb619137"
};

export const addToFirestore = async(obj) => {
  const collectionRef = firestore.collection('messages');
  const newdoc = collectionRef.doc();
  const batch = firestore.batch();

  batch.set(newdoc,obj);


  await batch.commit()
};

firebase.initializeApp(config)
export const auth = firebase.auth();
export const firestore = firebase.firestore();
export default !firebase.apps.length 
  ? firebase.initializeApp(config).firestore()
  : firebase.app().firestore();