import React from "react";
import {Link} from 'gatsby';

const links = [
    {
        id:1,
        text:'Home',
        url:"/"
    },
    {
        id:2,
        text:'About',
        url:"/about/"
    },
    {
        id:3,
        text:'Projects',
        url:"/projects"
    },
    {
        id:4,
        text:'Contact',
        url:"/contact/"
    },
    {
        id:5,
        text:'Login',
        url:"/login/"
    }
];

export default ({styleClass}) => {
    return (
        <ul className={`page-links ${styleClass ? styleClass : ""}`}>
            {
                links.map(link => 
                    <li key={link.id}>
                        <Link to={link.url}>{link.text}</Link>
                    </li>
                )
            }
        </ul>
    )
}